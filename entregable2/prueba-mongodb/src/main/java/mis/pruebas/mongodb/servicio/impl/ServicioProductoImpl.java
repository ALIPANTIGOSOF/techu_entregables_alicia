package mis.pruebas.mongodb.servicio.impl;

import mis.pruebas.mongodb.datos.RepositorioProducto;
import mis.pruebas.mongodb.modelo.Producto;
import mis.pruebas.mongodb.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import mis.pruebas.mongodb.servicio.ServicioProducto;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    public ServicioProductoImpl()  {
        System.out.println("===============ServicioProductoImpl");
    }
    @Autowired
    private RepositorioProducto repositorioProducto;

    @Override
    public Producto crearProducto(Producto producto) {
        return this.repositorioProducto.insert(producto);
    }

    @Override
    public List<Producto> obtenerProducto() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public Producto obtenerProductoPorId(String id) {
        final Optional<Producto> p = this.repositorioProducto.findById(id);
        return p.isPresent() ?  p.get() : null;
    }
    @Override
    public Producto actualizarProducto(String id, Producto productonew) {
        final Optional<Producto> p = this.repositorioProducto.findById(id);
        if(p.isPresent()){
            productonew.setId(id);
            this.repositorioProducto.save(productonew);
            return productonew;
        }
        return null;
    }

    @Override
    public void borraProductoPorid(String id) {
        this.repositorioProducto.deleteById(id);

    }
     @ Override
     public List<Usuario> obtenerUsuariosProducto(String idProducto) {
         final Producto p = this.obtenerProductoPorId(idProducto);
         if(p == null)
             throw new IllegalArgumentException("No existe el producto");
         final List<Usuario> usuarios = p.getUsuarios();
            return usuarios == null
                 ? Collections.emptyList()
                 : usuarios
                ;
      }
}

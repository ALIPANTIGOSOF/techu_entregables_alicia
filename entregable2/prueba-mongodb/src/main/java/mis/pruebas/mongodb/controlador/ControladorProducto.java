package mis.pruebas.mongodb.controlador;

import mis.pruebas.mongodb.modelo.Producto;
import mis.pruebas.mongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public List<Producto> obtenerProducto() {
        return this.servicioProducto.obtenerProducto();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto agregarProducto(@RequestBody Producto producto){
        return this.servicioProducto.crearProducto(producto);
    }
    @PutMapping("/{id}")
    public Producto actualizaProducto(@PathVariable String id, @RequestBody Producto productonew){
        final Producto p= this.servicioProducto.actualizarProducto(id, productonew);
        if (p == null)
            throw  new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
    @GetMapping("/{id}")
    public ResponseEntity<Producto> obtenerProducto(@PathVariable String id) {
        final Producto p = this.servicioProducto.obtenerProductoPorId(id);
        if (p != null) {
            return ResponseEntity.ok(p);
        }
        return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProducto(@PathVariable String id) {
        this.servicioProducto.borraProductoPorid(id);
    }
}

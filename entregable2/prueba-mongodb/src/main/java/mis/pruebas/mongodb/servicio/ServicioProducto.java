package mis.pruebas.mongodb.servicio;

import mis.pruebas.mongodb.modelo.Producto;
import mis.pruebas.mongodb.modelo.Usuario;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioProducto {
    //CRUD
     //POST /producto
    public Producto crearProducto(Producto producto);

    //GET /producto
    public List<Producto> obtenerProducto();

    //GET /productos/{id}
    public Producto obtenerProductoPorId(String id);

    //PUT /producto/{id} @RequestBody
    public Producto actualizarProducto( String id, Producto producto);

    //DELETE /producto/{id}
    public void borraProductoPorid(String id);

    //GET /producto/{idProducto}/usuarios
    public List<Usuario> obtenerUsuariosProducto(String idProducto);
}

package API_entregable1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechUEntregable1Application {

	public static void main(String[] args) {
		SpringApplication.run(TechUEntregable1Application.class, args);
	}

}

package API_entregable1.Controlador;

import API_entregable1.Servicio.ServicioDatos;
import API_entregable1.modelo.ModeloProducto;
import API_entregable1.modelo.ModeloUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorUsuariosProducto {
    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuariosProducto(@PathVariable int idProducto) {
        try {
            return ResponseEntity.ok (this.servicioDatos.obtenerUsuariosProducto(idProducto));
        } catch(Exception x){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity obtenerUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        final ModeloUsuario u = this.servicioDatos.obtenerusuarioProducto(idProducto, idUsuario);
        return (u == null)
                ? new ResponseEntity(HttpStatus.NOT_FOUND)
                : ResponseEntity.ok(u);
    }

    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity agregarUsuarioProducto(@PathVariable int idProducto, @RequestBody ModeloUsuario usuario){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(this.servicioDatos.agregarUsuarioProducto(idProducto, usuario));
    }

    @DeleteMapping("/{idProducto}/usuarios/{idUsuarios}")
    public ResponseEntity borraUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario){
        this.servicioDatos.borrarUsuarioProducto(idProducto, idUsuario);
        return new ResponseEntity("!Eliminado!",HttpStatus.NO_CONTENT);
    }


}

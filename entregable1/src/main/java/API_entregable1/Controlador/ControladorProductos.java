package API_entregable1.Controlador;

import API_entregable1.Servicio.ServicioDatos;
import API_entregable1.modelo.ModeloProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorProductos {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping
    public ResponseEntity obtenerProductos() {

        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    @PostMapping
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto) {
        final ModeloProducto p = this.servicioDatos.agregarProducto(producto);
        return ResponseEntity.ok(p);
    }

    @GetMapping("/{idProducto}")
    public ResponseEntity obtenerUnProducto(@PathVariable int idProducto) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p != null) {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{idProducto}")
    public ResponseEntity borrarUnProducto(@PathVariable int idProducto) {
        this.servicioDatos.borrarProducto(idProducto);
        return new ResponseEntity("!Eliminado!", HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{idProducto}")
    public ResponseEntity actualizarProducto(@PathVariable int idProducto, @RequestBody ModeloProducto producto) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        this.servicioDatos.actualizarProducto(idProducto, producto);
        return new ResponseEntity("!Actualizado!", HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{idProducto}")
    public ResponseEntity actualizarPrecio(@PathVariable int  idProducto,
                                           @RequestBody ModeloProducto productoPrecioOnly) {
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(idProducto);
        if (p == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        p.setPrecio(productoPrecioOnly.getPrecio());
        return (this.servicioDatos.actualizarProducto(idProducto,p))
                ? ResponseEntity.ok(p)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

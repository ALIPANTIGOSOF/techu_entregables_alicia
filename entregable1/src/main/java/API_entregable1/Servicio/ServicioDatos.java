package API_entregable1.Servicio;

import API_entregable1.modelo.ModeloProducto;
import API_entregable1.modelo.ModeloUsuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//CRUD de Productos
//Create
//Read
//Update th
//Delete

@Service
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos
            = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios
            = new AtomicInteger(0);
    private final List<ModeloProducto> productos
            = new ArrayList<ModeloProducto>();


    //CREATE
    public ModeloProducto agregarProducto(ModeloProducto producto) {
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }
    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario){
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    //READ colección
    public List<ModeloProducto> obtenerProductos() {
        return Collections.unmodifiableList(this.productos);
    }
    public List<ModeloUsuario> obtenerUsuariosProducto(int idProducto) {
        return Collections.unmodifiableList(this.obtenerProductoPorId(idProducto).getUsuarios());
    }

    //READ elemento
    public ModeloProducto obtenerProductoPorId(int idProducto) {
        for (ModeloProducto p : this.productos) {
            if (p.getId() == idProducto)
                return p;  //ojo: se puede modificar desde afuera
        }
        return null;
    }
    public ModeloUsuario obtenerusuarioProducto(int idProducto, int idUsuario) {
        final ModeloProducto p = this.obtenerProductoPorId(idProducto);
        if (p == null)
            return null;
        for (ModeloUsuario u: p.getUsuarios()){
            if (u.getId() == idUsuario){
                return u;
            }
        }
        return null;
    }

    //UPDATE
    public boolean actualizarProducto(int idProducto, ModeloProducto producto) {
        for (int i = 0; i < this.productos.size(); ++i) {
            if (this.productos.get(i).getId() == idProducto) {
                producto.setId(idProducto);
                this.productos.set(i, producto);
                return true;
            }
        }
        return false;
    }


    //DELETE
    public boolean borrarProducto(int idProducto) {
        for (int i = 0; i < this.productos.size(); ++i) {
            if (this.productos.get(i).getId() == idProducto) {
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean borrarUsuarioProducto(int idProducto, int idUsuario) {
        final ModeloProducto producto = this.obtenerProductoPorId(idProducto);

        if (producto == null)
            return false;
        final List<ModeloUsuario> usuarios = producto.getUsuarios();
        for (int i = 0; i < usuarios.size(); ++i) {

            if (usuarios.get(i).getId() == idUsuario) {
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}

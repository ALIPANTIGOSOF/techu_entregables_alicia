API REST para gestionar productos y sus usuarios acociados
Modelo
 - Producto
    Id : Int
    Marca: String
    Descripcion :String
    Precio : double
    Usuarios {}
  - Usuario
    id  :int
    Nombre:string
  /producto/  -> Coleccion de los productos  GET POST
  /producto/{id_prod} -> Un producto en particular GET PUT DELETE PATCH
  /producto/{id_prod}/usuarios/ -> Coleccion de los usuarios del producto {id} GET, POST
  /producto/{id_prod}/usuarios/{id_usuario} -> Usuario particular del producto {id_prod} GET DELETE PUT PATCH

   Modelo (Java)
   API_entregable1.Controlador REST (Spring)